class ChangeActivityLogRegNumtoTagNum < ActiveRecord::Migration
  def change
  	  	rename_column :activity_logs, :cattle_reg_num, :tagNum
  	  	rename_column :activity_logs, :activity_date, :activityDate

  end
end

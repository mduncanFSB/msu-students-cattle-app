class AddDescriptionToMeetings < ActiveRecord::Migration
  def change
    add_column :meetings, :description, :string
    add_column :meetings, :status, :string
    add_column :meetings, :complete, :int
  end
end

json.extract! task, :id, :taskName, :start_time, :status, :complete, :comment, :dateComplete, :created_at, :updated_at, :taskTagNumber, :tasksUnixDateTime
json.url task_url(task, format: :json)
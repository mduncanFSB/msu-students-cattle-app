class ChangeTagNumberColumnName < ActiveRecord::Migration
  def change
  	  	rename_column :tasks, :tagNumber, :taskTagNumber

  end
end

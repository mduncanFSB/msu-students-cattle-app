FactoryGirl.define do
  factory :task do
    name "MyString"
    start_time "2017-03-23 15:00:20"
    status "MyString"
    complete 1
    date_complete "2017-03-23 15:00:20"
  end
end

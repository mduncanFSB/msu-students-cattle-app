class FixStartsAt < ActiveRecord::Migration
  def change
  	rename_column :tasks, :starts_at, :start_time
  end
end

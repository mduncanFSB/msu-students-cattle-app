class FixColumnName < ActiveRecord::Migration
  def change
    change_table :cattle_stats do |t|
      t.rename :RegistrationNumber, :registrationNumber
      t.rename :Tagnumber, :tagNumber
      t.rename :Name, :new_column1
      t.rename :MaternalRegistrationNumber, :maternalRegistrationNumber
      t.rename :PaternalRegistrationNumber, :paternalRegistrationNumber
      t.rename :DOB, :dateOfBirth
      t.rename :Status, :status
      t.rename :SellDate, :sellDate
      t.rename :Location, :location
      t.rename :IsPregnant, :isPregnant
      t.rename :IsInHeat, :isInHeat
      t.rename :NextPregnancyCheck, :nextPregnancyCheck
      t.rename :LastPregnancyCheck, :lastPregnancyCheck
      t.rename :AnticipatedCalfBirthDate, :anticipatedCalfBirthDate
      t.rename :VaccinationDate, :vaccinationDate
    end
  end
end

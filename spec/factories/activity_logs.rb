FactoryGirl.define do
  factory :activity_log do
    user "MyString"
    task "MyString"
    activity_date 1
    cattle_reg_num 1
    status "MyString"
  end
end

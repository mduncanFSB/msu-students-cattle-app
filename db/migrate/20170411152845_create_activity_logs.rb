class CreateActivityLogs < ActiveRecord::Migration
  def change
    create_table :activity_logs do |t|
      t.string :user
      t.string :task
      t.integer :activity_date
      t.integer :cattle_reg_num
      t.string :status

      t.timestamps null: false
    end
  end
end

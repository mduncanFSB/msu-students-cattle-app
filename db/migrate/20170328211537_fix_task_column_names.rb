class FixTaskColumnNames < ActiveRecord::Migration
  def change
  	rename_column :tasks, :tag_number, :tagNumber
  	rename_column :tasks, :date_complete, :dateComplete
  	remove_column :tasks, :cow_id
  end
end

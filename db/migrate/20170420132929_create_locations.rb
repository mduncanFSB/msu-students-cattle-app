class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :longName
      t.string :shortname
      t.string :farmName

      t.timestamps null: false
    end
  end
end

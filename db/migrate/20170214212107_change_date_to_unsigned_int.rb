class ChangeDateToUnsignedInt < ActiveRecord::Migration
  def change
  	remove_column :cattle_stats, :dateOfBirth
  	remove_column :cattle_stats, :sellDate
  	remove_column :cattle_stats, :nextPregnancyCheck
  	remove_column :cattle_stats, :lastPregnancyCheck
  	remove_column :cattle_stats, :vaccinationDate
  	remove_column :cattle_stats, :anticipatedCalfBirthDate

	add_column :cattle_stats, :dateOfBirth, :integer
	add_column :cattle_stats, :sellDate, :integer
	add_column :cattle_stats, :nextPregnancyCheck, :integer
	add_column :cattle_stats, :lastPregnancyCheck, :integer
	add_column :cattle_stats, :vaccinationDate, :integer
	add_column :cattle_stats, :anticipatedCalfBirthDate, :integer



  	
  end
end

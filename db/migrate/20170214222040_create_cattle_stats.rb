class CreateCattleStats < ActiveRecord::Migration
  def change
    create_table :cattle_stats do |t|
      t.integer :registrationNumber
      t.integer :tagNumber
      t.string :name
      t.integer :maternalRegistrationNumber
      t.integer :paternalRegistrationNumber
      t.string :status
      t.string :location
      t.integer :isPregnant
      t.integer :isInHeat
      t.integer :weight
      t.integer :dateOfBirth
      t.integer :sellDate
      t.integer :nextPregnancyCheck
      t.integer :vaccinationDate
      t.integer :anticipatedBirthDate
      t.string :sex

      t.timestamps null: false
    end
  end
end

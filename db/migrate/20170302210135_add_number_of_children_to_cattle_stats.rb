class AddNumberOfChildrenToCattleStats < ActiveRecord::Migration
  def change
    add_column :cattle_stats, :numberOfChildren, :integer
  end
end

json.extract! cattle_stat, :id, :registrationNumber, :tagNumber, :cowName, :maternalRegistrationNumber, :paternalRegistrationNumber, :status, :location, :isPregnant, 
:heatWindowStartDate, :weight, :dateOfBirth, :sellDate, :nextPregnancyCheck, :vaccinationDate, :anticipatedBirthDate, :sex, :numberOfChildren, :created_at, 
:updated_at, :numOfStraws, :inHeat, :insemDate
json.url cattle_stat_url(cattle_stat, format: :json)

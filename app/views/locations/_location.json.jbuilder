json.extract! location, :id, :longName, :shortname, :farmName, :created_at, :updated_at
json.url location_url(location, format: :json)
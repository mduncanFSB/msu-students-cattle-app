FactoryGirl.define do
  factory :cattle_stat do
    registrationNumber 1
    tagNumber 1
    name "MyString"
    maternalRegistrationNumber 1
    paternalRegistrationNumber 1
    status "MyString"
    location "MyString"
    isPregnant 1
    isInHeat 1
    weight 1
    dateOfBirth 1
    sellDate 1
    nextPregnancyCheck 1
    vaccinationDate 1
    anticipatedBirthDate 1
    sex "MyString"
  end
end

class CattleStatsController < ApplicationController
  protect_from_forgery unless: -> { request.format.json? }
  before_action :set_cattle_stat, only: [:show, :edit, :update, :destroy]

  # GET /cattle_stats
  # GET /cattle_stats.json
  def index
    @cattle_stats = CattleStat.all
  end

  # GET /cattle_stats/1
  # GET /cattle_stats/1.json
  def show
  end

  # GET /cattle_stats/new
  def new
    @cattle_stat = CattleStat.new
  end

  # GET /cattle_stats/1/edit
  def edit
  end

  def task
    @cattle_stat = CattleStat.set_cattle_stat
  end

  # POST /cattle_stats
  # POST /cattle_stats.json



  def create
    @cattle_stat = CattleStat.new(cattle_stat_params)
    @cattle_stat.insemDate = 0
    
    #create new activity 
    @new_log = ActivityLog.new()
    @new_log.task = 'Add New Cattle'
    @new_log.user = current_user.name
    @new_log.activityDate = Time.now
    @new_log.status = 'Added cow'
    @new_log.tagNum = @cattle_stat.tagNumber

    #create base tasks for new cow.
    @task = Task.new()
    @task.taskName = 'Wean'
    @task.status = 'Ready To Wean'
    if @cattle_stat.dateOfBirth != nil
      @task.tasksUnixDateTime = @cattle_stat.dateOfBirth + (209 * 24 * 60 * 60)
    end
    @task.start_time = Time.at(@task.tasksUnixDateTime)
    @task.complete = 0
    @task.taskTagNumber = @cattle_stat.tagNumber
    @task.save


    respond_to do |format|
      if @cattle_stat.save
        #save log
        @new_log.save
        format.html { redirect_to @cattle_stat, notice: 'Cattle stat was successfully created.' }
        format.json { render :show, status: :created, location: @cattle_stat }
      else
        format.html { render :new }
        format.json { render json: @cattle_stat.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cattle_stats/1
  # PATCH/PUT /cattle_stats/1.json
  def update
    #create new activity 
    @new_log = ActivityLog.new()
    @new_log.task = 'Update Cattle'
    @new_log.user = 'temp'
    @new_log.activityDate = Time.now
    @new_log.status = 'updated cow'
    @new_log.tagNum = @cattle_stat.registrationNumber
    respond_to do |format|
      if @cattle_stat.update(cattle_stat_params)
        @new_log.save
        format.html { redirect_to @cattle_stat, notice: 'Cattle stat was successfully updated.' }
        format.json { render :show, status: :ok, location: @cattle_stat }
      else
        format.html { render :edit }
        format.json { render json: @cattle_stat.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cattle_stats/1
  # DELETE /cattle_stats/1.json
  def destroy
    @cattle_stat.destroy
    respond_to do |format|
      format.html { redirect_to cattle_stats_url, notice: 'Cattle stat was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cattle_stat
      @cattle_stat = CattleStat.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cattle_stat_params
      params.require(:cattle_stat).permit(:registrationNumber, :tagNumber, :maternalRegistrationNumber, :paternalRegistrationNumber, :status, :location, :isPregnant, :weight, :dateOfBirth, :sellDate, :nextPregnancyCheck, :vaccinationDate, :anticipatedBirthDate, :sex, :heatWindowStartDate, :cowName, :numOfStraws, :inHeat, :insemDate)
    end
end

class ChangeColumnName < ActiveRecord::Migration
  def change
    rename_column :cattle_stats, :isInHeat, :heatWindow
  end
end

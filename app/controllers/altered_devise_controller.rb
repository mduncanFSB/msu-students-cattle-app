class AlteredDeviseController::RegistrationController < Devise::RegistrationController
	
	private

  def sign_up_params
    params.require(:user).permit(:email, :name, :password, :location, :password_confirmation)
  end

  def account_update_params
    params.require(:user).permit(:email, :name, :password, :location, :password_confirmation)
  end


end

# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170420152619) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activity_logs", force: :cascade do |t|
    t.string   "user"
    t.string   "task"
    t.integer  "activityDate"
    t.integer  "tagNum"
    t.string   "status"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "ar_internal_metadata", primary_key: "key", force: :cascade do |t|
    t.string   "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cattle_stats", force: :cascade do |t|
    t.integer  "registrationNumber"
    t.integer  "tagNumber"
    t.string   "cowName"
    t.integer  "maternalRegistrationNumber"
    t.integer  "paternalRegistrationNumber"
    t.string   "status"
    t.string   "location"
    t.integer  "isPregnant"
    t.integer  "heatWindowStartDate"
    t.integer  "weight"
    t.integer  "dateOfBirth"
    t.integer  "sellDate"
    t.integer  "nextPregnancyCheck"
    t.integer  "vaccinationDate"
    t.integer  "anticipatedBirthDate"
    t.string   "sex"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "numberOfChildren"
    t.integer  "numOfStraws"
    t.integer  "inHeat"
    t.integer  "insemDate"
  end

  create_table "locations", force: :cascade do |t|
    t.string   "longName"
    t.string   "shortName"
    t.string   "farmName"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "meetings", force: :cascade do |t|
    t.string   "name"
    t.datetime "start_time"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "description"
    t.string   "status"
    t.integer  "complete"
  end

  create_table "tasks", force: :cascade do |t|
    t.string   "taskName"
    t.datetime "start_time"
    t.string   "status"
    t.integer  "complete"
    t.datetime "dateComplete"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "taskTagNumber"
    t.string   "comment"
    t.integer  "tasksUnixDateTime"
    t.datetime "end_time"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                             default: "", null: false
    t.string   "encrypted_password",                default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.string   "name"
    t.integer  "role"
    t.string   "location"
    t.string   "authentication_token",   limit: 30
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end

class TasksController < ApplicationController
  before_action :set_task, only: [:show, :edit, :update, :destroy, :all]

  # GET /tasks
  # GET /tasks.json
  def index
    @tasks = Task.all
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
  end

  def showAll
    @tasks = Task.all
  end

  # GET /tasks/new
  def new
    @task = Task.new
  end

  # GET /tasks/1/edit
  def edit
  end

  def cs
    #@task = Task.set_task
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @task = Task.new(task_params)
    @task.tasksUnixDateTime = @task.start_time.to_i

    #add default status
    if @task.taskName == 'Wean'
      @task.status = 'Ready To Wean'
    elsif @task.taskName == 'Weigh'
      #add default status based on days old
    elsif @task.taskName == 'Check In-Heat'
      @task.status = 'Not Ready To Breed'
    elsif @task.taskName == 'Well Check'
      @task.status = 'Birthing Watch'
    elsif @task.taskName == 'Vet Visit'
      @task.status = 'Injured'
    elsif @task.taskName == 'Vaccinate'
      @task.status = 'Birth Vaccination'
    elsif @task.taskName == 'Transfer'
      @task.status = 'Ready To Transfer'
    elsif @task.taskName == 'Pull Blood'
      @task.status = 'Pregnancy Check'
    elsif @task.taskName == 'Order Straws'
      @task.status = 'Order Straws'
    elsif @task.taskName == 'No Action Required'
      @task.status = 'All Is Well'
    elsif @task.taskName == 'Whole Herd Booster'
      @task.status = 'Whole Herd Booster'


    end 

    @task.tasksUnixDateTime = @task.start_time.to_i

    #crate new activity
    @new_log = ActivityLog.new()
    @new_log.task = 'Create new task:' + @task.taskName
    @new_log.user = current_user.name
    @new_log.activityDate = Time.now
    @new_log.status = @task.status
    @new_log.tagNum = @task.taskTagNumber
    
    respond_to do |format|
      if @task.save
        @new_log.save
        format.html { redirect_to @task, notice: 'Task was successfully created.' }
        format.json { render :show, status: :created, location: @task }
      else
        format.html { render :new }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def update
    respond_to do |format|
      if @task.update(task_params)
        
        #update task/CS for "weigh"
        if @task.taskName == 'Weigh'    
          #create CS object from tag number, update fields
          @cs = CattleStat.find_by tagNumber: @task.taskTagNumber
          @cs.weight = @task.comment
          @cs.status = @task.status

          #convert object to a hash and update
          csHash = @cs.as_json
          @cs.update(csHash)
         
          
        elsif @task.taskName == 'Whole Herd Booster'
          @t = Task.new()
          @t.taskName = 'Whole Herd Booster'
          @t.status = 'Whole Herd Booster'
         
          @t.tasksUnixDateTime = Time.now.to_i + (60 * 60 * 24 * 180)
          @t.start_time = Time.at(@t.tasksUnixDateTime)
          @t.taskTagNumber = '1'
          @t.save
        elsif @task.taskName == 'Check In-Heat' and @task.status != 'Ready To Breed'
          @t = Task.new()
          @t.taskName = 'Check In-Heat'
          @t.tasksUnixDateTime = Time.now.to_i + (60*60*24*16)
          @t.start_time = Time.at(@t.tasksUnixDateTime)
          @t.end_time = Time.at(@t.tasksUnixDateTime + (60*60*24*9))
          @t.taskTagNumber = @task.taskTagNumber
          @t.save
        elsif @task.taskName == 'Check In-Heat' and @task.status == 'Ready To Breed' and @task.complete == 1
          @cs = CattleStat.find_by tagNumber: @task.taskTagNumber

          @t = Task.new()
          @t.taskName = 'Breed'
          @t.status = 'Ready for Insemination'
          @t.comment = @task.comment
          @t.tasksUnixDateTime = Time.now.to_i + (60*60*24*16)
          @t.start_time = Time.at(@t.tasksUnixDateTime)
          @t.end_time = 0
          @t.taskTagNumber = @task.taskTagNumber
          @t.save

          @t2 = Task.new()
          @t2.taskName = 'Pull Blood'
          @t2.status = 'Pregnancy Check'
          @t2.tasksUnixDateTime = @cs.insemDate.to_i + (60*60*24*30)
          @t2.start_time = Time.at(@cs.insemDate.to_i + (60*60*24*30))
          @t2.end_time = 0
          @t2.taskTagNumber = @task.taskTagNumber
          @t2.save
        elsif @task.taskName == 'Pull Blood' and @task.status == 'Pregnant'
          @cs = CattleStat.find_by tagNumber: @task.taskTagNumber
          @cs.isPregnant = 1
          csHash = @cs.as_json
          @cs.update(csHash)          
          
          @t = Task.new()
          @t.taskName = 'Well Check'
          @t.status =  'Birthing Watch'
          @t.tasksUnixDateTime =  @cs.insemDate.to_i + (60*60*24*269)
          @t.start_time = Time.at(@t.tasksUnixDateTime)
          @t.end_time = Time.at(@t.tasksUnixDateTime.to_i + (60*60*24*20))
          @t.taskTagNumber = @task.taskTagNumber
          @t.save
        elsif @task.taskName == 'Breed' and @task.status == 'Inseminated'
          @cs = CattleStat.find_by tagNumber: @task.comment     
          @cs.numOfStraws = @cs.numOfStraws.to_i - 1
          csHash = @cs.as_json
          @cs.update(csHash) 
          puts @task.comment
          if @cs.numOfStraws < 3
            @straws = Task.new()
            @straws.taskName = 'Order Straws'
            @straws.start_time = Time.now
            @straws.tasksUnixDateTime = Time.now.to_i
            @straws.taskTagNumber = @task.comment
            @straws.save
          end 
        elsif @task.taskName == 'Order Straws' 
          @cs = CattleStat.find_by tagNumber: @task.taskTagNumber
          
          @cs.numOfStraws = @cs.numOfStraws+ @task.comment.to_i
          csHash = @cs.as_json
          @cs.update(csHash) 

    
          


        end
        
        #create new activity 
        @new_log = ActivityLog.new()
        @new_log.task = 'Update task: ' + @task.taskName
        @new_log.user = current_user.name
        @new_log.activityDate = Time.now
        @new_log.status = @task.status
        @new_log.tagNum = @task.taskTagNumber
        @new_log.save

        format.html { redirect_to @task, notice: 'Task was successfully updated.' }
        format.json { render :show, status: :ok, location: @task }
  
      else
        format.html { render :edit }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @task.destroy
    respond_to do |format|
      format.html { redirect_to tasks_url, notice: 'Task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @task = Task.find(params[:id])
     # @cattle_stat = CattleStat.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      params.require(:task).permit(:taskName, :start_time, :status, :comment, :complete, :dateComplete, :taskTagNumber, :tasksUnixDateTime, :end_time)
    end
end

class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :name
      t.datetime :start_time
      t.string :status
      t.integer :complete
      t.datetime :date_complete

      t.timestamps null: false
    end
  end
end

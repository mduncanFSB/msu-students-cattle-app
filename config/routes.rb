Rails.application.routes.draw do
  get '/tasks/showAll', to: 'tasks#showAll'
  get '/activity_logs/:name', to: 'activity_logs#byUser'
  get '/activity_logs/:tagNumber', to: 'activity_logs#byCow'


  #get '/activity_logs/byCow/:tagNum', to: 'activity_logs#byCow'


  resources :locations
  resources :activity_logs
  resources :tasks
  resources :cattle_stats
  resources :cattle_stats
  root to: 'tasks#index'
  get '/users/sign_up', to: 'tasks#index'
  devise_for :users
  
end

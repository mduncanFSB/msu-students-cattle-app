json.extract! activity_log, :id, :user, :task, :activityDate, :tagNum, :status, :created_at, :updated_at
json.url activity_log_url(activity_log, format: :json)